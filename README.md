# Leaderboard Backend

## Technologies
The project uses:
* [Loopback](https://loopback.io/) for working with node.js
* [MongoDB](https://www.mongodb.com/) for storing the data
* [Redis](https://redis.io/) for leaderboard related logics


## Routes

`GET /generate` - deletes all users and creates new users

`GET /leaderboard` - returns leaderboard data with random user

`GET /finishDay` - gives random money to every user and fast forwards to end of the day

`GET /finishWeek` - distributes prizepool to users

## Example User Json
```js
User:{
    name: string
    money: number
    country: string
    daily_rank: number  // for showing the daily_difference
    weekly_prize: number // for showing the earned prizePool money
}
``` 
## Example PrizePool Distrubition Code
``src/helpers/prizeFunctions.ts``
```ts
// Last user gets x% and First User gets y% where (x + y)/2 = 100
// So avarage prize remains 100% percent
// Ratio is firstPrize/lastPrize, if ratio=10 it means Last user gets 1x while First User gets 10x
export const calcLinearPerc = (userCount: number, index: number, ratio: number) => {
    let startPerc = 200 / (ratio + 1); // the percentage last user getting

    let unit = (200 - (2 * startPerc)) / (userCount - 1)

    let perc = startPerc + (unit * (userCount - 1 - index)) // index must start from 0

    return perc;
}
```