import {Entity, model, property} from '@loopback/repository';

@model()
export class User extends Entity {
  @property({
    type: 'string',
    id: true,
    generated: true
  })
  _id?: string;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'number',
    required: true,
  })
  money: number;

  @property({
    type: 'string',
    required: true,
  })
  country: string;

  @property({
    type: "number",
    required: true,
    default: 0,
  })
  weekly_prize: number

  @property({
    type: 'number',
    default: 0
  })
  daily_rank: number;


  constructor(data?: Partial<User>) {
    super(data);
  }
}

export interface UserRelations {
  // describe navigational properties here
}

export type UserWithRelations = User & UserRelations;
