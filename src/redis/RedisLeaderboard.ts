import { promisify } from "util";

const redis = require("redis");
const client = redis.createClient();

const get = promisify(client.get).bind(client);
const getRevRank = promisify(client.zrevrank).bind(client);
const getRevRange = promisify(client.zrange).bind(client);
const getCard = promisify(client.zcard).bind(client);
const getRandomMember = promisify(client.zrandmember).bind(client)
const zscore = promisify(client.zscore).bind(client);

export default class RedisLeaderboard {
    private key: string
    private prizePoolKey: string

    constructor(key = "leaderboard") {
        this.key = key;
        this.prizePoolKey = `${this.key}-prizepool`;
    }

    getScore = async(username: string) => {
        return await zscore([this.key, username])
    }

    add = (username: string, money: number) => {
        client.zadd([this.key, money, username], function (err: unknown) {
            if (err) throw err;
        })
    }

    remove = (user_id: string) => {
        client.zrem([this.key, user_id]), function (err: unknown) {
            if (err) throw err;
        }
    }

    increase = (user_id: string, money: number) => {
        client.zincrby([this.key, money, user_id]), function (err: unknown) {
            if (err) throw err;
        }
        // Can be negative value
    }

    delete = () => {
        client.del(this.key, function (err: unknown) {
            if (err) throw err;
        })
    }

    // Async functions
    userCount = async () => {
        let userCount = await getCard([this.key]);
        return userCount;
    }

    getRange = async (start: number, end: number) => {
        let args = [this.key, start - 1, end - 1, "WITHSCORES", "REV"];
        let users = await getRevRange(args)
        return users

    }

    getAll = async () => {
        let args = [this.key, 0, -1, "WITHSCORES", "REV"]
        let users = await getRevRange(args)
        return users

    }

    getRandomUser = async () => {
        let randomUser = await getRandomMember([this.key]);
        return randomUser;
    }

    getTop = (limit = 100) => {
        return this.getRange(1, limit);
    }

    getRank = async (username: string) => {
        let rank = await getRevRank([this.key, username]);;
        return rank + 1;
    }

    // PRIZEPOOL

    setPrizePool = (money: number) => {
        client.set(this.prizePoolKey, money)
    }

    increasePrizePool = (money: number) => {
        client.incrby(this.prizePoolKey, money)
    }

    getPrizePool = async () => {
        let prizePool = await get(this.prizePoolKey)
        return prizePool;
    }

    // getUserWithRanks = () => {

    // }
}