import { Filter, NumberType, repository } from '@loopback/repository';
import { get, response } from '@loopback/rest';

import { User } from '../models';
import { UserRepository } from '../repositories';
import { fakerUsers, randomInt } from '../helpers/fakerUsers';
import { getPercentage, getDeduction, calcLinearPerc } from '../helpers/prizeFunctions';

import RL from '../redis/RedisLeaderboard';

const Leaderboard = new RL();

export class LeaderboardController {
  constructor(
    @repository(UserRepository)
    public userRepository: UserRepository,
  ) { }

  // *************************************************************** // 
  // ******************** USER RELATED LOGICS ********************** // 
  // *************************************************************** // 

  // Returns User Range between (User's Rank - 3) and (User's Rank + 2)
  async getUserRange(username: string) {
    let userRank = await Leaderboard.getRank(username);

    let [start, end] = [userRank - 3, userRank + 2];
    if (start < 1) { start = 1 }

    let userRange = await Leaderboard.getRange(start, end);

    return userRange;
  }

  // Returns usernames array from a redis response 
  getUsernames(redisArray: []) {
    let usernames = redisArray.filter((element, index) => {
      return index % 2 == 0
    })
    return usernames
  }

  // Returns Users from Database using usernames array
  async getUsersWithDiff(usernamesArray: []) {

    let filter: Filter<User> = {
      where: { name: { inq: usernamesArray } },
      fields: ["name", "money", "country", "daily_rank", "weekly_prize"],
      order: ["money DESC", "name DESC"]
    };

    let users = await this.userRepository.find(filter)

    let startRank = await Leaderboard.getRank(users[0].name);

    let usersWithDiff = users.map((user, index) => ({
      ...user,
      daily_diff: user.daily_rank - (startRank + index)

    }))
    return usersWithDiff;
  }

  async getTop() {
    let topUsers = await Leaderboard.getTop();
    topUsers = await this.getUsernames(topUsers);
    topUsers = await this.getUsersWithDiff(topUsers);

    return topUsers
  }

  async updateDailyRank() {
    let redisUsers = await Leaderboard.getAll();

    let redisUserNames = this.getUsernames(redisUsers);

    redisUserNames.forEach((username, index) => {
      this.userRepository.updateAll(
        { daily_rank: index + 1 },
        { "name": username }
      )
    })
  }

  // USER ROUTES
  @get('/generate')
  @response(200, {
    description: "Deletes Users and generates new Ones",
    content: {
      "application/json": {
        schema: {
        },
      },
    },
  })
  async generate() {

    this.userRepository.deleteAll();
    Leaderboard.delete();
    Leaderboard.setPrizePool(0);

    const dummyUsers = fakerUsers(500);
    await this.userRepository.createAll(dummyUsers);

    const users = await this.userRepository.find({ fields: ["name", "money"] });

    await users.forEach(user => {
      Leaderboard.add(user.name, user.money)
    })

    await this.updateDailyRank();
    return "success"
  }

  @get("/getLeaderboard")
  @response(200, {
    description: "Returns a randomUser with range",
    content: {
      "application/json": {
        schema: {
        },
      },
    },
  })
  async getLeaderboard() {

    let dbUsers = await this.userRepository.find({});
    if (dbUsers.length < 1) {
      await this.generate()
    }

    let topUsers = await this.getTop();

    let randomUser = await Leaderboard.getRandomUser();
    let userRank = await Leaderboard.getRank(randomUser);

    let usersRange = await this.getUserRange(randomUser);
    usersRange = this.getUsernames(usersRange);
    let users = await this.getUsersWithDiff(usersRange);

    if (userRank < 104) {
      let limit = 104 - userRank;
      for (let i = 0; i < limit; i++) {
        users.shift();
      }
    }

    let prizePool = await Leaderboard.getPrizePool();

    return {
      user: {
        name: randomUser,
        rank: userRank
      },
      range: users,
      prizePool: prizePool,
      topUsers: topUsers
    }
  }

  // *************************************************************** // 
  // ******************** PRIZE RELATED LOGICS ********************* // 
  // *************************************************************** // 

  async giveMoney(username: string, amount: number, type?: string) {
    let weekly_prize = type ? amount : 0
    this.userRepository.updateAll(
      {
        //@ts-ignore
        $inc: { money: amount },
        $set: { weekly_prize: weekly_prize }
      },
      { "name": username },
    )
  }

  async giveMoneyWithDeduction(username: string, amount: number) {
    let money = getDeduction(amount, 2);

    Leaderboard.increasePrizePool(money.deduction);

    Leaderboard.increase(username, money.remaining);

    this.giveMoney(username, money.remaining);
  }

  async giveRandomMoney() {
    let users = await Leaderboard.getAll();
    let usernames = await this.getUsernames(users);

    usernames.forEach(username => {
      this.giveMoneyWithDeduction(username, randomInt(3));
    })
  }

  getPrize(index: number, total: number) {
    let prize = 0;
    let remaining = getPercentage(total, 55)
    let avg = Math.round(remaining / 97)

    if (index > 2) {
      let ratio = 8 // Last User gets 1x Prize, First user gets 8x Prize
      let ind = index - 3;
      let perc = calcLinearPerc(97, ind, ratio);
      prize = getPercentage(avg, perc);

    } else {
      if (index == 0) {
        prize = getPercentage(total, 20);
      } else if (index == 1) {
        prize = getPercentage(total, 15);
      } else if (index == 2) {
        prize = getPercentage(total, 10);
      }
    }
    return prize;
  }
  // PRIZE ROUTES
  @get('/finishDay')
  @response(200, {
    description: "Gives random money to users and, fast forwards to the end of the day",
    content: {
      "application/json": {
        schema: {
        },
      },
    },
  })
  // Sets the starting rank to all users in the beginning of the day
  async finishDay() {
    this.updateDailyRank();
    this.giveRandomMoney();
  }

  @get("/finishWeek")
  @response(200, {
    description: "Distributes Weekly Prizes to Users",
    content: {
      "application/json": {
        schema: {
        },
      },
    },
  })
  async finishWeek() {
    const prizePool = await Leaderboard.getPrizePool();
    const topUsers = await this.getTop();
    let prizes: Number[] = [];

    topUsers.forEach(async (user: any, index: number) => {
      let prize = this.getPrize(index, prizePool);
      await this.giveMoney(user.name, prize, "prize");
      Leaderboard.increase(user.name, prize);
      prizes.push(prize)
    });

    Leaderboard.setPrizePool(0);

    await this.updateDailyRank();

    return prizes;
  }
}
