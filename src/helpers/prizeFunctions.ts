export const getPercentage = (number: number, percent: number) => {
    return Math.round(number * percent / 100);
}

// Cuts specific percent of the amount and returns remaining with deduction
export const getDeduction = (amount: number, percent: number) => {
    let deduction = getPercentage(amount, percent);
    let remaining = amount - deduction
    return {
        remaining: remaining,
        deduction: deduction
    }
}

// Last user gets x% and First User gets y% where (x + y)/2 = 100
// So avarage prize remains 100% percent
// Ratio is firstPrize/lastPrize, if ratio=10 it means Last user gets 1x while First User gets 10x
export const calcLinearPerc = (userCount: number, index: number, ratio: number) => {
    let startPerc = 200 / (ratio + 1); // the percentage last user getting

    let unit = (200 - (2 * startPerc)) / (userCount - 1)

    let perc = startPerc + (unit * (userCount - 1 - index)) // index must start from 0

    return perc;
}