const faker = require("faker");

export const randomInt = (digit: number) => Math.round(Math.random() * 10 ** digit)

export const fakerUser = (): { name: string, country: string, money: number } => {
    return {
        name: faker.internet.userName(),
        country: faker.address.countryCode().toLowerCase(),
        money: randomInt(5)
    }
}

export const fakerUsers = (limit: number): Array<{ name: string, country: string, money: number }> => {
    return Array.from({ length: limit }, fakerUser);
}